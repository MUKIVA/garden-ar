using UnityEngine;

public interface IUnpackStrategy
{
    public void Unpack(GameObject targetView, ProjectModel project);
}
