using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectPlane : MonoBehaviour
{
    private const string TREES_RESOURCES_PATH = "Prefabs/GardenObjects/Trees";
    private const string SHRUBS_RESOURCES_PATH = "Prefabs/GardenObjects/Shrubs";
    private const string BEDS_RESOURCES_PATH = "Prefabs/GardenObjects/Beds";
    private const string FLOWERS_RESOURCES_PATH = "Prefabs/GardenObjects/Flowers";
    private const string DECORATIONS_RESOURCES_PATH = "Prefabs/GardenObjects/Decor";

    public GameObject InitObjectOnPlane(PlatformObjectModel objModel)
    {
        GameObject prefab = GetPrefab(objModel.Name);
        GameObject obj = Instantiate(prefab, transform);
        obj.transform.localPosition = objModel.Position;
        obj.transform.localScale = objModel.Scale;
        obj.transform.rotation = Quaternion.Euler(objModel.Rotation);
        obj.layer = LayerMask.NameToLayer("PlatformObject");
        return obj;
    }

    private GameObject GetPrefab(string name)
    {
        GameObject obj = Resources.Load($"{TREES_RESOURCES_PATH}/{name}") as GameObject;
        if (obj != null)
            return obj;
        obj = Resources.Load($"{SHRUBS_RESOURCES_PATH}/{name}") as GameObject;
        if (obj != null)
            return obj;
        obj = Resources.Load($"{BEDS_RESOURCES_PATH}/{name}") as GameObject;
        if (obj != null)
            return obj;
        obj = Resources.Load($"{FLOWERS_RESOURCES_PATH}/{name}") as GameObject;
        if (obj != null)
            return obj;
        obj = Resources.Load($"{DECORATIONS_RESOURCES_PATH}/{name}") as GameObject;
        if (obj != null)
            return obj;

        throw new Exception("The resource does not exist");
    }

}
