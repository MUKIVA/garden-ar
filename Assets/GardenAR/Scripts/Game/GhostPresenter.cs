using System.Collections;
using UnityEngine;
using Vuforia;

public class GhostPresenter : MonoBehaviour
{
    [SerializeField] private GameObject _presentedObject;

    private static Color _fadeColor = new Color(1, 1, 1, 0.5f);

    public void ChangePosition(Vector3 hit)
    {
        transform.position
            = hit;
    }

    public void SetObject(GameObject obj)
    {
        _presentedObject = obj;
        ChangePreview(obj);
    }

    private void ChangePreview(GameObject obj)
    {
        foreach (Transform child in transform)
            Destroy(child.gameObject);

        GameObject previewObject = Instantiate(obj, transform);
        previewObject.transform.localPosition = Vector3.zero;
        previewObject.layer = gameObject.layer;
        Renderer[] renderers = previewObject.GetComponentsInChildren<Renderer>();
        //StartCoroutine(ChangeMaterial(renderers));
    }

    private IEnumerator ChangeMaterial(Renderer[] renderers)
    {
        foreach (Renderer renderer in renderers)
        {
            foreach (var material in renderer.materials)
            {
                material.mainTexture = Texture2D.whiteTexture;
                //material.SetupMaterialWithBlendMode(MaterialExt.BlendMode.Transparent);
                material.color = material.color * _fadeColor;
                yield return new WaitForFixedUpdate();
            }
        }
    }
}
