using System;
using UnityEngine;

public class RotatebleObject : MonoBehaviour
{
    [SerializeField] private float _sensivity = 0.25f;

    private GestureRecognizer _gesture;
    private Vector2 _startRotate;

    private void Start()
    {
        _gesture = GestureRecognizer.Instance;
        _gesture.Down += HandleDown;
        _gesture.Drag += HandleDrag;
        _gesture.Up += HandleUp;
    }

    private void HandleDown(Vector2 direction, Vector2 delta)
    {
        _startRotate = new (transform.rotation.x, transform.rotation.y); 
    }

    private void HandleDrag(Vector2 direction, Vector2 delta)
    {
        HandleRotate(direction, delta);
    }

    private void HandleUp(Vector2 direction, Vector2 delta)
    {
        //HandleRotate(direction, delta);
    }

    private void HandleRotate(Vector2 direction, Vector2 delta)
    {
        if (MathF.Abs(Vector2.Dot(Vector2.up, direction)) < 0.5f)
        {
            transform.Rotate(0, _startRotate.y - delta.x * _sensivity, 0);
        }
    }

    private void OnDestroy()
    {
        if (_gesture == null)
            return;

        _gesture.Down -= HandleDown;
        _gesture.Drag -= HandleDrag;
        _gesture.Up -= HandleUp;
    }
}
