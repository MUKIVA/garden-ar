using UnityEngine;

public class FadeUnpack : IUnpackStrategy
{
    private static Color _fadeColor = new Color(1, 1, 1, 0.5f);

    public void Unpack(GameObject targetView, ProjectModel project)
    {
        if (project == null) return;

        var projectPlane = targetView.GetComponent<ProjectPlane>();
        foreach (var item in project.Objects)
        {
            GameObject platformObj = projectPlane.InitObjectOnPlane(item);
            ChangeMaterials(platformObj);
        }
    }

    private void ChangeMaterials(GameObject obj)
    {
        Renderer[] renderers = obj.GetComponentsInChildren<Renderer>();
        //ChangeMaterial(renderers);
    }

    private void ChangeMaterial(Renderer[] renderers)
    {
        foreach (Renderer renderer in renderers)
        {
            foreach (var material in renderer.materials)
            {
                material.mainTexture = Texture2D.whiteTexture;
                //material.SetupMaterialWithBlendMode(MaterialExt.BlendMode.Transparent);
                material.color = material.color * _fadeColor;
            }
        }
    }
}
