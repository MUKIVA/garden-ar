using System.Linq;
using UnityEngine;

public enum UnpackStrategyEnum
{
    Default,
    Fade
}

public class ProjectUnpacker : MonoBehaviour
{
    public UnpackStrategyEnum UnpakStrategyEnum = UnpackStrategyEnum.Default;

    private IUnpackStrategy UnpackStrategy { get; set; } = new DefaultUnpack();

    public void UnpackProject()
    {
        UnpackStrategy.Unpack(gameObject, AppModel.ProjectModel);
    }

    private void Awake()
    {
        switch (UnpakStrategyEnum)
        {
            case UnpackStrategyEnum.Default: UnpackStrategy = new DefaultUnpack(); break;
            case UnpackStrategyEnum.Fade: UnpackStrategy = new FadeUnpack(); break;
            default: UnpackStrategy = new DefaultUnpack(); break;
        }

        UnpackProject();
    }
}
