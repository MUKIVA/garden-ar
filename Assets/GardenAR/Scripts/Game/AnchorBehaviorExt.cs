using Unity.VisualScripting;
using UnityEngine;
using Vuforia;

public static class AnchorBehaviorExt
{
    private static int Zero { get; set; } = 0;
    public static Transform GetTransformWrapper(this AnchorBehaviour sender)
        => sender.transform.GetChild(Zero);
}
