using System;
using UnityEngine;

public class ScaleableObject : MonoBehaviour
{
    [SerializeField] private float _sensivity = 0.0025f;

    private GestureRecognizer _gesture;
    private Vector3 _startScale;

    private void Start()
    {
        _gesture = GestureRecognizer.Instance;
        _gesture.Down += HandleDown;
        _gesture.Drag += HandleDrag;
        _gesture.Up += HandleUp;
    }

    private void HandleDown(Vector2 direction, Vector2 delta)
    {
        _startScale = transform.localScale;
    }

    private void HandleDrag(Vector2 direction, Vector2 delta)
    {
        HandleScale(direction, delta);
    }

    private void HandleUp(Vector2 direction, Vector2 delta)
    {
        //HandleRotate(direction, delta);
    }

    private void HandleScale(Vector2 direction, Vector2 delta)
    {
        if (MathF.Abs(Vector2.Dot(Vector2.left, direction)) < 0.5f)
        {
            var scale = new Vector3(
                delta.y * _sensivity,
                delta.y * _sensivity,
                delta.y * _sensivity);
            transform.localScale = _startScale + scale;
            _startScale = transform.localScale;
        }
    }

    private void OnDestroy()
    {
        if (_gesture == null)
            return;
        _gesture.Down -= HandleDown;
        _gesture.Drag -= HandleDrag;
        _gesture.Up -= HandleUp;
    }
}
