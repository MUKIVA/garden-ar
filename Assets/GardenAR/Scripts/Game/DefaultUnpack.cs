using UnityEngine;

public class DefaultUnpack : IUnpackStrategy
{
    public void Unpack(GameObject targetView, ProjectModel project)
    {
        if (project == null) return;
        
        var projectPlane = targetView.GetComponent<ProjectPlane>();
        foreach (var item in project.Objects)
        {
            projectPlane.InitObjectOnPlane(item);
        }
    }
}
