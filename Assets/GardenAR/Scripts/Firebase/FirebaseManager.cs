using Firebase;
using Firebase.Database;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using UnityEngine;

public static class FirebaseManager
{
    private const string PROJECT_PATH = "Project";
    private const int TIME_OUT = 5000;
    private const string FIREBASE_URI = "https://gardenar-9dbe5-default-rtdb.firebaseio.com/";
    private static bool Initialized { get; set; } = false;

    public static async Task Initialize()
    {
        await FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                var app = FirebaseApp.DefaultInstance;
                Initialized = true;
                Debug.Log("Firebase inited");
            }
            else
            {
                Debug.LogError($"Could not resolve all Firebase dependencies: {dependencyStatus}");
            }
        });
    }

    public static async Task<ProjectModel> GetProjectByKey(string key)
    {
        if (!Initialized)
            await Initialize();
        FirebaseApp.DefaultInstance.Options.DatabaseUrl = new Uri(FIREBASE_URI);

        var reference = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance).RootReference;

        reference = reference.Child(PROJECT_PATH).Child(key);
        var getProjectTask = reference.GetValueAsync();

        if (await Task.WhenAny(getProjectTask, Task.Delay(TIME_OUT)) == getProjectTask)
        {
            Debug.Log("[FirebaseManager] Done, succefull connecting to db and getting Project");

            var dataSnapshot = getProjectTask.Result;
            string json = dataSnapshot.GetRawJsonValue();

            if (json == null)
                return null;

            var project = JsonConvert.DeserializeObject<ProjectModel>(json, new JsonSerializerSettings()
            {
                Converters = new[]
                {
                    new Vec3Conv()
                }
            });

            return project;
        }

        Debug.Log("[GameEventsManager] DATABASE TIMEOUT in an attempt to load Project");

        return null;   
    }

    public static async Task SaveProject(ProjectModel project)
    {
        if (!Initialized)
            await Initialize();
        FirebaseApp.DefaultInstance.Options.DatabaseUrl = new Uri(FIREBASE_URI);
        var reference = FirebaseDatabase.GetInstance(FirebaseApp.DefaultInstance).RootReference;
        var json = JsonConvert.SerializeObject(project, new JsonSerializerSettings()
        {
            Converters = new[]
            {
                new Vec3Conv()
            }
        });

        await reference.Child(PROJECT_PATH).Child(project.Key).SetRawJsonValueAsync(json);
        Debug.Log("Added project to database: " + project.Key);

    }
}
