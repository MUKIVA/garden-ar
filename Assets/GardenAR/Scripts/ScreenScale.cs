﻿using UnityEngine;

public static class ScreenScale
{
    public static int EthalonScreenWidth => 2280;
    public static int EthalonScreenHeight => 1080;
    public static int CurrentScreenWidth => Screen.width;
    public static int CurrentScreenHeight => Screen.height;
    public static float CurrentWidthScreenRatio => CurrentScreenWidth / (float)EthalonScreenWidth;
    public static float CurrentHeightScreenRatio => CurrentScreenHeight / (float)EthalonScreenHeight;
}

