﻿using UnityEngine;
using Vuforia;

namespace Assets.Models
{
    public class CreatePlaceState : IAppState
    {
        public CreatePlaceState(AppModel context)
        {
            context.ContentPositionBehaviour.gameObject.SetActive(true);
            context.PreviewContainer.gameObject.SetActive(true);
            context.PlatformPreviewContainer.gameObject.SetActive(false);
            context.GameUIController.EnterPlacePlatformMode();
        }
        public void PlaceObject(AppModel context, HitTestResult hit)
        {
            if (hit is null) return;

            var transformWrapper = context.PlaneStage.GetTransformWrapper();
            transformWrapper.rotation = context.PreviewContainer.transform.rotation;
            transformWrapper.localScale = context.PreviewContainer.transform.localScale;
            context.ContentPositionBehaviour.PositionContentAtPlaneAnchor(hit);

            context.ChangeState(new ViewingState(context));
        }

        public void PlaceObjectOnPlatform(AppModel context) { }

        public void SetHitTest(AppModel context, HitTestResult hit)
        {
            context.PreviewContainer.ChangePosition(hit.Position);
        }
        public void Update(AppModel context) { }
    }
}
