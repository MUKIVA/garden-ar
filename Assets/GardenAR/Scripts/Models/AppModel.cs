using Assets.Models;
using UnityEngine;
using UnityEngine.Animations;
using Vuforia;

public class AppModel : MonoBehaviour
{
    public static ProjectModel ProjectModel { get; set; } = null;

    [SerializeField] private LayerMask _platformLayer;
    [SerializeField] private LayerMask _platformObjectLayer;
    [SerializeField] private GestureRecognizer _gestureRecognizer;
    [SerializeField] private AnchorBehaviour _planeStage;
    [SerializeField] private GhostPresenter _previewContainer;
    [SerializeField] private GhostPresenter _platformPreviewContainer;
    [SerializeField] private ContentPositioningBehaviour _contentPositioningBehaviour;
    [SerializeField] private Transform _camera;
    [SerializeField] private GameUIController _uiController;

    private GameObject _selectedObject;
    private static GameObject _currentPlatform;
    private IAppState _currentState;

    #region Props

    public GameUIController GameUIController => _uiController;
    public GestureRecognizer GestureRecognizer => _gestureRecognizer;
    public AnchorBehaviour PlaneStage => _planeStage;
    public GhostPresenter PreviewContainer => _previewContainer;
    public GhostPresenter PlatformPreviewContainer => _platformPreviewContainer;
    public ContentPositioningBehaviour ContentPositionBehaviour => _contentPositioningBehaviour;
    public Transform Camera => _camera;
    public GameObject SelectedObject => _selectedObject;
    public GameObject CurrentPlatform => _currentPlatform;
    public HitTestResult HitTestResult { get; private set; } = null;
    public LayerMask PlatformObjectLayer => _platformObjectLayer;
    public LayerMask PlatformLayer => _platformLayer;


    #endregion

    private void Awake()
    {
        _currentState = new CreatePlaceState(this);
    }

    private void Start()
    {
        _gestureRecognizer.Down += HandleTap;
    }

    private void FixedUpdate()
    {
        _currentState.Update(this);
    }

    public void PlaceObject()
    {
        _currentState.PlaceObject(this, HitTestResult);
    }

    public void PlaceObjectOnPlatform()
    {
        _currentState.PlaceObjectOnPlatform(this);
    }

    public void SetHitTest(HitTestResult hitTest)
    {
        HitTestResult = hitTest;
        _currentState.SetHitTest(this, hitTest);
    }

    public void ChangeState(IAppState state)
    {
        _currentState = state;
    }

    public void SelectObject(GameObject obj)
    {
        _selectedObject = null;
        ChangeState(new PlaceObjectState(this));
        PlatformPreviewContainer.SetObject(obj);
        _selectedObject = obj;
    }

    public void SetPlatform(GameObject platform)
    {
        Debug.Log($"Set Platform: {platform.GetComponent<AnchorBehaviour>().GetTransformWrapper().GetChild(0).gameObject.name}");
        _currentPlatform = platform.GetComponent<AnchorBehaviour>().GetTransformWrapper().GetChild(0).gameObject;
    }

    public void DeleteObject()
    {
        _currentState = new ViewingState(this);   
    }

    private Vector3 _origin = Vector3.zero;
    private Vector3 _direction = Vector3.up;


    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(_origin, _direction * 10);
        Gizmos.DrawWireSphere(_origin + _direction * 10, 0.25f);
    }

    public void HandleTap(Vector2 pos, Vector2 delta)
    {
        if (_currentState is ViewingState)
        {
            var centerPos = new Vector2(
                pos.x - (Screen.width / 2), pos.y - (Screen.height / 2));

            var hit = Physics.SphereCast(
                _camera.transform.position, 0.05f,
                (_camera.forward + new Vector3(centerPos.x / (Screen.width / 2), centerPos.y / (Screen.height / 2), 0)).normalized,
                out RaycastHit hitInfo,float.MaxValue, PlatformObjectLayer);
            Debug.Log(hit);
            if (hit)
            {
                var collider = hitInfo.collider;
                var platformObject = collider.gameObject;
                var copy = Instantiate(platformObject, Vector3.one * 9999, Quaternion.identity);
                copy.transform.localScale = Vector3.one;
                SelectObject(copy);
                Destroy(platformObject);
            }
        }
    }

    public static void UpdateProjectModel()
    {
        if (_currentPlatform == null) return;
        ProjectModel.Objects.Clear();

        foreach (Transform obj in _currentPlatform.transform)
        {
            ProjectModel.Objects.Add(new PlatformObjectModel()
            {
                Name = obj.gameObject.name.Replace("(Clone)", ""),
                Position = obj.localPosition,
                Scale = obj.localScale,
                Rotation = obj.localRotation.eulerAngles
            });
        }
    }
}
