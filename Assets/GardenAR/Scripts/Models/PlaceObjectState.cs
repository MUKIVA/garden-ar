﻿using UnityEngine;
using Vuforia;

namespace Assets.Models
{
    public class PlaceObjectState : IAppState
    {
        private RaycastHit _hit;

        public PlaceObjectState(AppModel context)
        {
            context.ContentPositionBehaviour.gameObject.SetActive(false);
            context.PreviewContainer.gameObject.SetActive(false);
            context.PlatformPreviewContainer.gameObject.SetActive(true);
            context.GameUIController.EnterPlaceMode();
        }
        public void PlaceObject(AppModel context, HitTestResult hit) 
        {
            PlaceObjectOnPlatform(context);
        }

        public void PlaceObjectOnPlatform(AppModel context)
        {
            if (context.PlatformPreviewContainer.gameObject.activeSelf && 
                context.CurrentPlatform != null)
            {
                GameObject obj = GameObject.Instantiate(context.SelectedObject, _hit.point, Quaternion.identity);
                obj.transform.rotation = context.PlatformPreviewContainer.gameObject.transform.rotation;
                obj.transform.localScale = context.PlatformPreviewContainer.gameObject.transform.localScale;
                obj.transform.SetParent(context.CurrentPlatform.transform);
                obj.layer = LayerMask.NameToLayer("PlatformObject");
                context.ChangeState(new ViewingState(context));
            }
        }

        public void SetHitTest(AppModel context, HitTestResult hit) { }
        public void Update(AppModel context)
        {

            SetPreviewVisible(context);
        }

        private void SetPreviewVisible(AppModel context)
        {
            context.PlatformPreviewContainer.gameObject
                .SetActive(Physics.Raycast(new Ray(
                    context.Camera.position, context.Camera.forward), out RaycastHit hit, float.MaxValue, context.PlatformLayer));
            context.PlatformPreviewContainer.ChangePosition(hit.point);
            _hit = hit;
        }
    }
}
