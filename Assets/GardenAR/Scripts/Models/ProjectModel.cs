using System;
using System.Collections.Generic;

[Serializable]
public class ProjectModel
{
    public string Key { get; set; } = Guid.NewGuid().ToString();
    public string Name { get; set; } = string.Empty;
    public DateTime CreateTime { get; set; } = DateTime.Now;
    public List<PlatformObjectModel> Objects { get; set; } = new();
}
