using UnityEngine;
using Vuforia;

public interface IAppState
{
    public void PlaceObject(AppModel context, HitTestResult hit);
    public void PlaceObjectOnPlatform(AppModel context);
    public void SetHitTest(AppModel context, HitTestResult hit);
    public void Update(AppModel context);
}
