﻿using Vuforia;

namespace Assets.Models
{
    public class ViewingState : IAppState
    {
        public ViewingState(AppModel context)
        {
            context.ContentPositionBehaviour.gameObject.SetActive(false);
            context.PreviewContainer.gameObject.SetActive(false);
            context.PlatformPreviewContainer.gameObject.SetActive(false);
            context.GameUIController.EnterViewMode();
        }

        public void PlaceObject(AppModel context, HitTestResult hit) {}
        public void PlaceObjectOnPlatform(AppModel context) {}
        public void SetHitTest(AppModel context, HitTestResult hit) {}
        public void Update(AppModel context) {}
    }
}
