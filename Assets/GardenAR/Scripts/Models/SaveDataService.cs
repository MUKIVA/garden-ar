using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

public static class SaveDataService
{
    public const string PROJECTS_FILE_NAME = "ProjectsData.dat";
    public static string PROJECTS_PATH = $"{Application.persistentDataPath}/{PROJECTS_FILE_NAME}";

    public static void SaveProjects(List<ProjectModel> projects)
    {
        bool fileExist = File.Exists(PROJECTS_PATH);

        projects.Reverse();

        var json = JsonConvert.SerializeObject(projects, new JsonSerializerSettings() 
        {
            Converters = new[] 
            {
                new Vec3Conv()
            } 
        });
        using (FileStream fs = new FileStream($"{Application.persistentDataPath}/{PROJECTS_FILE_NAME}", (fileExist) ? FileMode.Truncate : FileMode.OpenOrCreate))
        {
            byte[] buffer = Encoding.Default.GetBytes(json);
            fs.Write(buffer, 0, buffer.Length);
        }
    }

    public static ProjectModel[] LoadProjects()
    {
        string json;
        using (FileStream fs = new FileStream($"{Application.persistentDataPath}/{PROJECTS_FILE_NAME}", FileMode.OpenOrCreate))
        {
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            json = Encoding.Default.GetString(buffer);
        }

        if (string.IsNullOrEmpty(json)) return new ProjectModel[0];

        var data = JsonConvert.DeserializeObject<ProjectModel[]>(json, new JsonSerializerSettings()
        {
            Converters = new[]
            {
                new Vec3Conv()
            }
        });
        return data;
    }
}

public class Vec3Conv : JsonConverter
{
    public override bool CanConvert(Type objectType)
    {
        if (objectType == typeof(Vector3))
        {
            return true;
        }
        return false;
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        var t = serializer.Deserialize(reader);
        var iv = JsonConvert.DeserializeObject<Vector3>(t.ToString());
        return iv;
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        Vector3 v = (Vector3)value;

        writer.WriteStartObject();
        writer.WritePropertyName("x");
        writer.WriteValue(v.x);
        writer.WritePropertyName("y");
        writer.WriteValue(v.y);
        writer.WritePropertyName("z");
        writer.WriteValue(v.z);
        writer.WriteEndObject();
    }
}
