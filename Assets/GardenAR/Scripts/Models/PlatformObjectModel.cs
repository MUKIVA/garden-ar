using System;
using UnityEngine;

[Serializable]
public class PlatformObjectModel
{
    public string Name { get; set; } = string.Empty;
    public Vector3 Position { get; set; } = Vector3.zero;
    public Vector3 Rotation { get; set; } = Vector3.zero;
    public Vector3 Scale { get; set; } = Vector3.zero;
}
