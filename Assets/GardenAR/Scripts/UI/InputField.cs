using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InputField : MonoBehaviour
{
    [SerializeField] TMP_InputField _input;
    [SerializeField] TMP_Text _attentionText;
    [SerializeField] TMP_Text _headerText;

    public string Text
    {
        get => _input.text;
        set => _input.text = value;
    }

    public void SetHeader(string text)
    {
        _headerText.text = text;
    }

    public void SetAttention(string text)
    {
        _attentionText.text = text;
    }

    public void HideAttention()
    {
        _attentionText.gameObject.SetActive(false);
    }

    public void ShowAttention()
    {
        _attentionText.gameObject.SetActive(true);
    }

    
}
