﻿using DG.Tweening;
using UnityEngine;

namespace Assets.GardenAR.Scripts.UI
{
    public class OpenablePanel : MonoBehaviour
    {
        private RectTransform _rect;
        private const float ANIMATION_DURATION = 0.5f;
        private IOpenPanelStrategy _openStrategy = new RightOpenStrategy();

        private void Awake()
        {
            _rect = GetComponent<RectTransform>();
        }

        public void Open()
        {
            _openStrategy.Open(_rect, ANIMATION_DURATION);
        }

        public void Close()
        {
            _openStrategy.Close(_rect, ANIMATION_DURATION);
        }
    }
}
