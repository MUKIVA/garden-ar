using Assets.GardenAR.Scripts.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenFriendsProjLayout : OpenablePanel
{
    [SerializeField] InputField _inputKey;

    public async void LoadProject()
    {
        var project = await FirebaseManager.GetProjectByKey(_inputKey.Text);

        if (project == null)
        {
            SetAttentionMessage();
            return;
        }

        AppModel.ProjectModel = project;
        SceneManager.LoadScene(1);

    }

    public void SetAttentionMessage()
    {
        _inputKey.SetAttention("Key doesn�t exist");
        _inputKey.ShowAttention();
    }
}
