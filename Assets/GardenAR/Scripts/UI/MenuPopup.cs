using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPopup : Popup
{
    private const float NOTIFY_TIME = 5f;

    [SerializeField] private GameObject _confirmPopup;

    public async void SaveActionAsync()
    {
        await SaveAction(true);
    }

    public async Task SaveAction(bool hide = true)
    {
        AppModel.UpdateProjectModel();

        try
        {
            await FirebaseManager.SaveProject(AppModel.ProjectModel);
        }
        catch
        {
            ToastManager.Instance.ShowToast(NOTIFY_TIME, "������!");
        }

        SaveDataService.SaveProjects(ProjectList.Projects);
        ToastManager.Instance.ShowToast(NOTIFY_TIME, "������ ��������!");

        if (hide)
            Hide();
    }

    public void ExitAction()
    {
        gameObject.SetActive(false);
        _confirmPopup.SetActive(true);
    }

    public void CloseAction()
    {
        Hide();
    }

    public async void Exit(int exitCode)
    {
        _confirmPopup.SetActive(false);
        gameObject.SetActive(true);

        if (exitCode == 0)
        {
            SceneManager.LoadScene(0);    
        }

        if (exitCode == 1)
        {
            await SaveAction(false);
            SceneManager.LoadScene(0);
        }
    }
}
