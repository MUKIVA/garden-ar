using UnityEngine;

public class GameUIController : MonoBehaviour
{
    [SerializeField] private GameObject _menuButton;
    [SerializeField] private GameObject _createButton;
    [SerializeField] private GameObject _deleteButton;
    [SerializeField] private GameObject _okButton;
    [SerializeField] private GameObject _cancelButton;

    [SerializeField] private GameObject _firstStepGide;
    [SerializeField] private GameObject _controlsGide;

    public void EnterViewMode()
    {
        HideAll();
        ShowMenuButton();
        ShowCreateButton();
    }
    public void EnterPlaceMode()
    {
        HideAll();
        ShowOkButton();
        ShowMenuButton();
        ShowDeleteButton();
        _controlsGide.SetActive(true);
    }
    public void EnterPlacePlatformMode()
    {
        HideAll();
        ShowOkButton();
        ShowMenuButton();
        _firstStepGide.SetActive(true);
        _controlsGide.SetActive(true);
    }
    public void HideCancelButton() => _cancelButton.SetActive(false);
    public void ShowCancelButton() => _cancelButton.SetActive(true);
    public void HideOkButton() => _okButton.SetActive(false);
    public void ShowOkButton() => _okButton.SetActive(true);
    public void HideDeleteButton() => _deleteButton.SetActive(false);
    public void ShowDeleteButton() => _deleteButton.SetActive(true);
    public void HideMenuButton() => _menuButton.SetActive(false);
    public void ShowMenuButton() => _menuButton.SetActive(true);
    public void HideCreateButton() => _createButton.SetActive(false);
    public void ShowCreateButton() => _createButton.SetActive(true);
    public void HideAllButtons()
    {
        HideCreateButton();
        HideMenuButton();
        HideDeleteButton();
        HideOkButton();
        HideCancelButton();
    }
    public void ShowAllButtons()
    {
        ShowCreateButton();
        ShowMenuButton();
        ShowDeleteButton();
        ShowOkButton();
        ShowCancelButton();
    }
    public void HideAll()
    {
        HideAllButtons();
        _controlsGide.SetActive(false);
        _firstStepGide.SetActive(false);
    }
}
