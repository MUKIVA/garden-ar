using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CategoryButton : MonoBehaviour
{
    [SerializeField] Image _image;
    [SerializeField] Button _button;

    public void AddEvent(UnityAction action)
    {
        _button.onClick.AddListener(action);
    }

    public void SetImage(Texture2D image)
    {
        Sprite sprite = Sprite.Create(
            image, 
            new Rect(0, 0, image.width, image.height), 
            new Vector2(0.5f, 0.5f));
        _image.sprite = sprite;
        _image.useSpriteMesh = true;
        _image.preserveAspect = true;
    }
}
