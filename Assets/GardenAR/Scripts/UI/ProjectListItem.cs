using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ProjectListItem : MonoBehaviour
{
    private Action _deleteProject;
    private Action _openProject;
    private Action _renameProject;

    private DateTime _createDateTime;

    [SerializeField] TextMeshProUGUI _projectName;
    [SerializeField] TextMeshProUGUI _createDateLabel;
    [SerializeField] TMP_Dropdown _dropdown;
    [SerializeField] Image _preview; 

    public string ProjectName
    {
        get => _projectName.text;
        set
        {
            _projectName.text = value;
        }
    }

    public void FixedUpdate()
    {
        HandleDate();   
    }

    private void HandleDate()
    {
        var span = DateTime.Now - _createDateTime;

        if (span.Days > 365)
        {
            _createDateLabel.text = $"{span.Days} ��� �����";
            return;
        }

        if (span.Days > 31)
        {
            _createDateLabel.text = $"{span.Days / 31} �. �����";
            return;
        }

        if (span.Days > 0)
        {
            _createDateLabel.text = $"{span.Days} �. �����";
            return;
        }

        if (span.Hours > 0)
        {
            _createDateLabel.text = $"{span.Hours} �. �����";
            return;
        }

        if (span.Minutes > 0)
        {
            _createDateLabel.text = $"{span.Minutes} ���. �����";
            return;
        }

        if (span.Seconds > 0)
        {
            _createDateLabel.text = $"{span.Seconds} ���. �����";
            return;
        }
    }

    public void InitActions(
        Action deleteProject,
        Action openProject,
        Action renameProject,
        DateTime createDate)
    {
        _deleteProject = deleteProject;
        _renameProject = renameProject;
        _openProject = openProject;
        _createDateTime = createDate;
    }

    public void HandleMenuAction(int action)
    {
        if (action == 0) return;
        switch (action)
        {
            case 1: _renameProject(); break;
            case 2: _deleteProject(); break;
            default: break;
        }

        _dropdown.value = 0;
    }

    public void OpenProject()
    {
        _openProject();
    }

    public void SetPreview(Sprite image)
    {
        _preview.sprite = image;
    }
}
