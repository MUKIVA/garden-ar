using System;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmDeletePopup : Popup
{
    private Action _cancel = () => { };
    private Action _ok = () => { };

    public void InitActions(Action cancel, Action ok)
    {
        _cancel = cancel;
        _ok = ok;
        base.InitRoot();
    }

    public void Cancel()
    {
        _cancel();
        Hide();
    }

    public void Ok()
    {
        _ok();
        Hide();
    }
}
