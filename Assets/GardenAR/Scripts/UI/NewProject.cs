using Assets.GardenAR.Scripts.UI;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NewProject : OpenablePanel
{
    [SerializeField] TMP_InputField _inputField;

    public void CreateAndOpenProject()
    {
        var newProj = new ProjectModel
        {
            Name = _inputField.text
        };
        AppModel.ProjectModel = newProj;
        ProjectList.Projects.Add(newProj);
        SaveDataService.SaveProjects(ProjectList.Projects);
        SceneManager.LoadScene(1);
    }
}
