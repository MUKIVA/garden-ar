using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollRectLock : MonoBehaviour
{
    void Awake()
    {
        var scrollRect = GetComponent<ScrollRect>();
        scrollRect.vertical = false;
        scrollRect.horizontal = false;
    }
}
