﻿using DG.Tweening;
using UnityEngine;

namespace Assets.GardenAR.Scripts.UI
{
    public class RightOpenStrategy : IOpenPanelStrategy
    {
        public void Close(RectTransform transform, float duration)
        {
            transform.DOLocalMoveX(0, duration, true)
                .SetEase(Ease.InOutCubic);
        }

        public void Open(RectTransform transform, float duration)
        {
            transform.DOLocalMoveX(-transform.rect.width, duration, true)
                .SetEase(Ease.InOutCubic);
        }
    }
}
