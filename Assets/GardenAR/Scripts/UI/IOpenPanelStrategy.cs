﻿using UnityEngine;

namespace Assets.GardenAR.Scripts.UI
{
    public interface IOpenPanelStrategy
    {
        public void Open(RectTransform transform, float duration);
        public void Close(RectTransform transform, float duration);
    }
}
