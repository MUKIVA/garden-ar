using UnityEngine;
using DG.Tweening;
using TMPro;
using System.Collections.Generic;

public struct SelectPanelData
{
    public string Header { get; set; }
    public GameObject Panel { get; set; }
}

public class SelectObjectController : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI _headerText;
    [SerializeField] GameObject _categoryGrid;
    [SerializeField] GameObject _objectGrid;
    [SerializeField] GameObject _backButton;

    private RectTransform _rect;
    private Stack<SelectPanelData> _history = new();
    private const float ANIMATION_DURATION = 0.5f; 

    private void Awake()
    {
        _rect = GetComponent<RectTransform>();   
    }

    public void ChangeHeader(string text)
    {
        _headerText.text = text;
    }

    public void ShowSelectPanel()
    {
        _rect.DOMoveY(_rect.rect.height * ScreenScale.CurrentWidthScreenRatio, ANIMATION_DURATION)
            .SetEase(Ease.InOutCubic);
    }
    public void HideSelectPanel()
    {
        _rect.DOMoveY(0, ANIMATION_DURATION)
            .SetEase(Ease.InOutCubic);
    }

    public void ClaerHystory()
    {
        _history.Clear();
        _objectGrid.SetActive(false);
        UpdateBackButtonActive();     
    }

    public void OpenCategory()
    {
        PushHistory(_categoryGrid);
        UpdateBackButtonActive();
    }

    public void OpenObjects(string header)
    {
        PushHistory(_objectGrid, header);
        UpdateBackButtonActive();
    }

    public void Back()
    {
        PopHistory();
        UpdateBackButtonActive();       
    }

    private void UpdateBackButtonActive()
    {
        _backButton.SetActive(_history.Count > 1);
    }

    private void PushHistory(GameObject grid, string header = "�������� ���������")
    { 
        if (_history.Count != 0)
        {
            var peek = _history.Peek();
            peek.Panel.SetActive(false);
        }

        _history.Push(new SelectPanelData() 
        {
            Header = header,
            Panel = grid
        });
        ChangeHeader(header);
        grid.SetActive(true);        
    }

    private void PopHistory()
    {
        if (_history.Count <= 1) return;

        var current = _history.Pop();
        current.Panel.SetActive(false);

        var peek = _history.Peek();
        ChangeHeader(peek.Header);
        peek.Panel.SetActive(true);
    }
}
