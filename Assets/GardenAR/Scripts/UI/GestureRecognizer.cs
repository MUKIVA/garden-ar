using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void DragEvent(Vector2 gestureDirection, Vector2 gestureDelta);
public delegate void DownEvent(Vector2 gestureDirection, Vector2 gestureDelta);
public delegate void UpEvent(Vector2 gestureDirection, Vector2 gestureDelta);

[RequireComponent(typeof(Image))]
public class GestureRecognizer : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    private static Vector2 _pointerPosition;
    private static Vector2 _gestureDelta;
    private DragEvent _dragEvent;
    private DownEvent _downEvent;
    private UpEvent _upEvent;

    public static GestureRecognizer Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        var image = GetComponent<Image>();
        image.color = new Color(0, 0, 0, 0);
    }

    public event DragEvent Drag
    {
        add => _dragEvent += value;
        remove => _dragEvent -= value;
    }

    public event DownEvent Down
    {
        add => _downEvent += value;
        remove => _downEvent -= value;
    }

    public event UpEvent Up
    {
        add => _upEvent += value;
        remove => _upEvent -= value;
    }

    public void OnDrag(PointerEventData eventData)
    {
        HandlePointerDrag(eventData.position);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        HandlePointerDown(eventData.position);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        HandlePointerUp(eventData.position);
    }

    private void HandlePointerDown(Vector2 position)
    {
        _pointerPosition = position;
        _gestureDelta = Vector2.zero;
        _downEvent?.Invoke(position, _gestureDelta);
    }

    private void HandlePointerUp(Vector2 position)
    {
        _gestureDelta = new(
            position.x - _pointerPosition.x,
            position.y - _pointerPosition.y);
        _pointerPosition = position;
        _upEvent?.Invoke(_gestureDelta.normalized, _gestureDelta);
    }

    private void HandlePointerDrag(Vector2 position)
    {
        _gestureDelta = new(
            position.x - _pointerPosition.x,
            position.y - _pointerPosition.y);
        _pointerPosition = position;
        _dragEvent?.Invoke(_gestureDelta.normalized, _gestureDelta);        
    }
}
