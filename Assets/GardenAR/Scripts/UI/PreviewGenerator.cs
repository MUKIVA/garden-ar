using UnityEngine;

public class PreviewGenerator : MonoBehaviour
{
    [SerializeField] private Transform _previewSpawnPoint;
    [SerializeField] private Camera _renderCamera;

    private const float ETHALON_HEIGHT = 6f;
    private const float ETHALON_WIDTH = 1.2f; 
    public Transform SpawnPoint => _previewSpawnPoint;
    public Camera RenderCamera => _renderCamera;

    private void ClearSpawnPoint()
    {
        foreach (Transform item in _previewSpawnPoint)
        {
            item.gameObject.SetActive(false);
            Destroy(item.gameObject);
        }        
    }

    private void SpawnObjectOnSpawnPoint(GameObject obj, bool changeSize = true)
    {
        var preview = Instantiate(obj, _previewSpawnPoint);
        preview.transform.localPosition = Vector3.zero;
        if (changeSize)
            ChangeSizeToEthalon(preview);
        //preview.transform.localScale = Vector3.one;
        preview.layer = gameObject.layer;
        ChangeLayer(preview.transform);
    }

    private void ChangeLayer(Transform tr)
    {
        foreach (Transform item in tr)
        {
            ChangeLayer(item);
            item.gameObject.layer = gameObject.layer;
        }
    }

    private void ChangeSizeToEthalon(GameObject obj)
    {
        float maxHeight = 0f;
        Collider[] colliders = obj.GetComponentsInChildren<Collider>();
        foreach (Collider collider in colliders)
        {
            var height = collider.bounds.size.y;
            var width = collider.bounds.size.x;
            if (maxHeight < height && width <= ETHALON_WIDTH)
                maxHeight = height;
        }

        obj.transform.localScale = obj.transform.localScale * ((maxHeight == 0) 
            ? 1f 
            : ETHALON_HEIGHT / maxHeight);
    }

    public Texture2D GenerateTexture(GameObject obj, bool changeSize = true)
    {
        var cameraTexture = RenderCamera.targetTexture;
        Texture2D texture = new Texture2D(
            cameraTexture.width, 
            cameraTexture.height,
            TextureFormat.ARGB32, false
            );
        ClearSpawnPoint();
        SpawnObjectOnSpawnPoint(obj, changeSize);
        RenderCamera.Render();
        Graphics.CopyTexture(cameraTexture, texture);
        return texture;
    }    
}
