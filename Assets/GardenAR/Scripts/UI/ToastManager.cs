using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using System.Threading.Tasks;

public class ToastManager : MonoBehaviour
{
    [SerializeField] GameObject _toast;
    [SerializeField] TMP_Text _toastText;
    private RectTransform _toastRectTransform;
    private const float TOAST_TOP_MARGIN = 25f;
    private const float DURATION = 0.5f;

    public static ToastManager Instance { get; private set; } = null;

    private void Awake()
    {
        _toastRectTransform = _toast.GetComponent<RectTransform>();
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void ShowToast(float time, string text)
    {
        _toastText.text = text;
        Debug.Log(_toastRectTransform.rect.height);
        DOTween.Sequence()
            .Append(_toast.transform.DOLocalMoveY(-_toastRectTransform.rect.height - TOAST_TOP_MARGIN, DURATION).SetEase(Ease.InOutCubic))
            .AppendInterval(time)
            .Append(_toast.transform.DOLocalMoveY(0, DURATION).SetEase(Ease.InOutCubic));
    }
}
