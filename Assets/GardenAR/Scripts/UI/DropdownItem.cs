using UnityEngine;
using UnityEngine.EventSystems;

public delegate void DropdownItemClick();

public class DropdownItem : MonoBehaviour, IPointerClickHandler
{
    private DropdownItemClick _dropdownItemClick;

    public event DropdownItemClick OnDropdownItemClick
    {
        add => _dropdownItemClick += value;
        remove => _dropdownItemClick -= value;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _dropdownItemClick?.Invoke();
    }
}
