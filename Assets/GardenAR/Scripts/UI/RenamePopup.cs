using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RenamePopup : Popup
{
    [SerializeField] TMP_InputField _input;
    private Action _cancelAction;
    private Action<string> _okAction;
    [SerializeField] Button _okButton;

    public void InitActions(
        Action cancelAction,
        Action<string> okAction)
    {
        _cancelAction = cancelAction;
        _okAction = okAction;
        InitRoot();
    }

    public void Cancel()
    {
        _cancelAction();
        Hide();
    }

    public void Ok()
    {
        _okAction(_input.text);
        Hide();
    }

    public void OnValueChange(string value)
    {
        _okButton.interactable = value != string.Empty;   
    }
}
