using UnityEngine;
using UnityEngine.UI;

public class MainMenuLayout : MonoBehaviour
{
    public void ExitApp() => Application.Quit();
}
