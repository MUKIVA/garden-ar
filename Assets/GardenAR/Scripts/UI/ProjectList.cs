using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProjectList : MonoBehaviour
{
    [SerializeField] ProjectListItem _projectListItem;
    [SerializeField] GameObject _projectPrefab;
    [SerializeField] Transform _content;
    [SerializeField] GameObject _emptyView;
    [SerializeField] ConfirmDeletePopup _deletePopup;
    [SerializeField] RenamePopup _renamePopup;
    [SerializeField] PreviewGenerator _generator;

    private static List<ProjectModel> _projects;

    public static List<ProjectModel> Projects => _projects;

    public void LoadProjects()
    {
        //_projects = new List<ProjectModel>()
        //{
        //    new()
        //    {
        //        Name = "First project",
        //        Objects = new()
        //        {
        //            new()
        //            {
        //                Name = "bigLeaves",
        //                Position = Vector3.right * 3,
        //                Rotation = Vector3.zero,
        //                Scale = Vector3.one * 0.5f
        //            }
        //        }
        //    }
        //};
        _projects = SaveDataService.LoadProjects().Reverse().ToList();
    }

    public void Start()
    {
        LoadProjects();
        //_emptyView.SetActive(_projects.Count <= 0);
        StartCoroutine(nameof(CreateProjectsView));
    }

    public IEnumerator CreateProjectsView()
    {
        foreach (var proj in _projects)
        {
            GameObject item = Instantiate(_projectListItem.gameObject, _content);
            ProjectListItem projItem = item.GetComponent<ProjectListItem>();
            projItem.ProjectName = proj.Name;
            SetViewImage(projItem, proj);
            projItem.InitActions(
                deleteProject: () => DeleteProject(proj, item),
                renameProject: () => RenameProject(proj, projItem),
                openProject: () =>
                {
                    AppModel.ProjectModel = proj;
                    SceneManager.LoadScene(1);
                }, createDate: proj.CreateTime);
            yield return new WaitForFixedUpdate();
        }
    }

    private void SetViewImage(ProjectListItem item, ProjectModel projectModel)
    {
        ProjectPlane projectPlane = Instantiate(_projectPrefab).GetComponent<ProjectPlane>();
        foreach (var projItem in projectModel.Objects)
        {
            projectPlane.InitObjectOnPlane(projItem);
        }
        projectPlane.gameObject.transform.rotation = Quaternion.Euler(25, 45, 25);
        projectPlane.gameObject.transform.localScale = Vector3.one;
        var image = _generator.GenerateTexture(projectPlane.gameObject, false);
        item.SetPreview(Sprite.Create(
            image,
            new Rect(0, 0, image.width, image.height),
            new Vector2(0.5f, 0.5f)
            ));
        Destroy(projectPlane.gameObject);
    }

    private void CopyProjectKeyToBuffer(ProjectModel proj)
    {
        GUIUtility.systemCopyBuffer = proj.Key;
    }

    private void DeleteProject(ProjectModel proj, GameObject view)
    {
        _deletePopup.InitActions(
            ok: () =>
            {
                _projects.Remove(proj);
                Destroy(view);
                //_emptyView.SetActive(_projects.Count <= 0);
                SaveDataService.SaveProjects(_projects);
            },
            cancel: () => { });
        _deletePopup.Show();
    }

    private void RenameProject(ProjectModel proj, ProjectListItem item)
    {
        _renamePopup.InitActions(
            cancelAction: () => {},
            okAction: (newProjectName) => 
            {
                proj.Name = newProjectName;
                item.ProjectName = newProjectName;
                SaveDataService.SaveProjects(_projects);
            });
        _renamePopup.Show();
    }

}
