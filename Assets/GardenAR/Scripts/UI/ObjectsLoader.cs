using System.Collections;
using UnityEngine;

public class ObjectsLoader : MonoBehaviour
{

    [SerializeField] Transform _content;
    [SerializeField] PreviewGenerator _previewGenerator;
    [SerializeField] GameObject _buttonTemplate;
    [SerializeField] SelectObjectController _selectObjectController;
    [SerializeField] AppModel _appModel;

    private const string TREES_RESOURCES_PATH = "Prefabs/GardenObjects/Trees";
    private const string SHRUBS_RESOURCES_PATH = "Prefabs/GardenObjects/Shrubs";
    private const string BEDS_RESOURCES_PATH = "Prefabs/GardenObjects/Beds";
    private const string FLOWERS_RESOURCES_PATH = "Prefabs/GardenObjects/Flowers";
    private const string DECORATIONS_RESOURCES_PATH = "Prefabs/GardenObjects/Decor";

    private IEnumerator ClearAll()
    {
        foreach (Transform item in _content)
        {
            Destroy(item.gameObject);
        }
        yield return null;
    }

    private IEnumerator LoadFromResources(string path)
    {
        yield return StartCoroutine(ClearAll());
        GameObject[] prefabs = Resources.LoadAll<GameObject>(path);
        foreach (var item in prefabs)
        {
            Texture2D image = _previewGenerator.GenerateTexture(item);
            CategoryButton button = Instantiate(_buttonTemplate, _content).GetComponent<CategoryButton>();
            button.SetImage(image);
            button.AddEvent(_selectObjectController.HideSelectPanel);
            button.AddEvent(() => _appModel.SelectObject(item));
            yield return new WaitForEndOfFrame();
        }
    }

    public void LoadTrees()
    {
        StopAllCoroutines();
        StartCoroutine(LoadFromResources(TREES_RESOURCES_PATH));
    }   

    public void LoadShrubs()
    {
        StopAllCoroutines();
        StartCoroutine(LoadFromResources(SHRUBS_RESOURCES_PATH));
    }

    public void LoadBeds()
    {
        StopAllCoroutines();
        StartCoroutine(LoadFromResources(BEDS_RESOURCES_PATH));
    } 

    public void LoadFlowers()
    {
        StopAllCoroutines();
        StartCoroutine(LoadFromResources(FLOWERS_RESOURCES_PATH));
    }

    public void LoadDecorations()
    {
        StopAllCoroutines();
        StartCoroutine(LoadFromResources(DECORATIONS_RESOURCES_PATH));
    }
}
