using System;
using Unity.VisualScripting;
using UnityEngine;

public class Popup : MonoBehaviour
{
    protected PopupLayout _root;

    public void InitRoot()
    {
        _root = GetComponentInParent<PopupLayout>(true);

        if (_root == null)
            throw new Exception("Popup must be located in PopupLayout");
    }

    public virtual void Show()
    {
        if (gameObject.IsDestroyed()) return;

        gameObject?.SetActive(true);
        _root?.Show();
    }

    public virtual void Hide()
    {
        if (gameObject.IsDestroyed()) return;

        gameObject.SetActive(false);
        _root?.Hide();
    }
}
