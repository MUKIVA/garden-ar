Shader "Unlit/GradientShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _Angle ("Angle", Range(0, 6.28 )) = 0
        _PrimaryColor ("Primary color", Color) = (0,0,0,0)
        _SecondaryColor ("Secondary color", Color) = (1,1,1,1)
        _Smoothness ("Smoothness", float) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            float _Angle;
            half4 _PrimaryColor;
            half4 _SecondaryColor;
            float _Smoothness;
            float4 _MainTex_ST;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float2 uv = i.uv - 0.5;
                float s = sin(_Angle);
                float c = cos(_Angle);
                float2x2 rotation = float2x2(c, -s, s, c);
                uv = mul(rotation, uv);
                half4 col = lerp(_PrimaryColor, _SecondaryColor, smoothstep(-_Smoothness, _Smoothness, uv.x));
                return col;
            }
            ENDCG
        }
    }
}
